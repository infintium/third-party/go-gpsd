package gpsd

import (
	"log"
	"os"
	"strconv"
)

// Logger gives ability to use different logger implementations.
type Logger interface {
	Errorf(format string, v ...interface{})
	Debugf(format string, v ...interface{})
}

func newEnvLogger() *envLogger {
	return &envLogger{
		l: log.New(os.Stderr, "[gpsd] ", 0),
		d: debugFromEnv(),
	}
}

type envLogger struct {
	l *log.Logger
	d bool
}

func (l *envLogger) Errorf(format string, v ...interface{}) {
	l.l.Printf("E " + format, v...)
}


func (l *envLogger) Debugf(format string, v ...interface{}) {
	if l.d {
		l.l.Printf("D " + format, v...)
	}
}

func debugFromEnv() bool {
	debug0, _ := strconv.ParseBool(os.Getenv("DEBUG"))
	debug1, _ := strconv.ParseBool(os.Getenv("DEBUG_GPSD"))
	return debug0 || debug1
}

